# b64codec
A small and lightweight library written in C to handle base 64 encoding and decoding.

### What's this?
The idea sparked out from a project I've been working on months ago. While reviewing the old code just for fun, the base64
encoding thing catched my eye and I wanted to dive a bit deeper into the matter. Therefore I wrote my own base64 encoder/decoder,
it might not be as effecient as many others, but at least I guess it could help other people understand better.

### How to set it up?
b64codec is pretty much easy to use and setup. Just add it to your project or link it against your executable, and you're done!

``` c
uint8_t b64enc(const uint8_t *wrd, const size_t wrdlen, uint8_t *b64wrd, const size_t b64maxlen, size_t *b64wrdlen)
```
This function allows base 64 encoding of a char array into another one. The first argument is a pointer to the array of chars
you'd like to encode. Second argument is the length (null-terminating char included) of such array. Third one is the address
of the char array where you'd want the encoded data to be stored. Fourth is the maximum lenght of this second array. The last
one is the address of an int where the actual length of the encoded array will be stored (null-terminating char included).
If for whatever reason the encoded word length overcomes the respective maximum array length, then function returns value 1.
Otherwise, value 0 is returned.

``` c
uint8_t b64dec(const uint8_t *b64wrd, const size_t b64wrdlen, uint8_t *wrd, const size_t maxlen, size_t *wrdlen)
```
This function allows base 64 decoding of a char array into another one. The first argument is a pointer to the array of chars
you'd like to decode. Second argument is the length (null-terminating char included) of such array. Third one is the address
of the char array where you'd want the decoded data to be stored. Fourth is the maximum lenght of this second array. The last
one is the address of an int where the actual length of the decoded array will be stored (null-terminating char included).
If the encoded word length (null-terminating char excluded) is not a multiple of 4, then function returns value 1. If for
whatever reason the decoded word length overcomes the respective maximum array length, then function returns value 2.
Otherwise, value 0 is returned.
